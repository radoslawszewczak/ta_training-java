package com.epam.training.radoslaw_szewczak;

public class MaxMethod {
    public static int max(int[] values) {
        int maxValue = values[0];
        for (int value:values) {
            if (value > maxValue){
                maxValue = value;
            }
        }
        return  maxValue;
    }
}
